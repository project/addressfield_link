<?php
/**
 * Implements hook_ctools_plugin_directory().
 */
function addressfield_link_ctools_plugin_directory($module, $plugin) {
  if ($module == 'addressfield') {
    return 'plugins/' . $plugin;
  }
}

/**
 * Implements hook_field_attach_presave().
 *
 * Store additional address fields serialized in addressfield data column.
 */
function addressfield_link_field_attach_presave($entity_type, $entity) {
  $addressfields = addressfield_link_addressfield_types();
  foreach ($addressfields as $field_name) {
    if (isset($entity->$field_name)) {
      $field = field_info_field($field_name);
      list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
      $instance = field_info_instance($entity_type, $field_name, $bundle);
      $default_fields = array_keys(addressfield_default_values($field, $instance));
      $default_fields[] = 'element_key';
      foreach ($entity->$field_name as $lang_code => $fields) {
        foreach ($fields as $delta => $fieldset) {
          $data = array();
          foreach ($fieldset as $field_key => $field_value) {
            // Compare fields with addressfield default fields and store any
            // additional fields in data.
            if (!in_array($field_key, $default_fields)){
              // Store additional field value
              $data[$field_key] = $field_value;
            }
          }
          if (!empty($data)){
            $entity->{$field_name}[$lang_code][$delta]['data'] = serialize($data);
          }
        }
      }
    }
  }
}

/**
 * Implements hook_field_attach_load().
 *
 * Load and set additional addressfield field values from serialized data
 * column.
 */
function addressfield_link_field_attach_load($entity_type, $entities, $age, $options){
  $addressfields = addressfield_link_addressfield_types();
  $link_elements = drupal_map_assoc(array(
    'link_title',
    'link_url',
  ));

  foreach ($entities as $entity) {
    foreach ($addressfields as $field_name) {
      if (empty($entity->{$field_name})) {
        continue;
      }

      list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
      $instance = field_info_instance($entity_type, $field_name, $bundle);

      if ($instance['widget']['settings']['format_handlers']['link'] === 'link') {
        foreach ($entity->$field_name as $lang_code => $fields) {
          foreach ($fields as $delta => $fieldset) {
            // Fill default values for pre-addressfield_link addresses to keep
            // Commerce Order from duplicating address.
            if (empty($fieldset['data'])) {
              $fieldset['data'] = serialize(array_fill_keys($link_elements, ''));
            }

            // Unserialize data, else skip if string
            if (($data = unserialize($fieldset['data'])) && is_array($data)) {
              // Store unserialized data values in additional fields
              $link_data = array_intersect_key($data, $link_elements);
              $entity->{$field_name}[$lang_code][$delta] = array_merge($fieldset, $link_data);
            }
          }
        }
      }
    }
  }
}

/**
 * Returns an array of the field names of any addressfields.
 */
function addressfield_link_addressfield_types() {
  static $addressfields;
  if (!isset($addressfields)) {
    $result = db_query("SELECT field_name FROM {field_config} WHERE type = 'addressfield'");
    $addressfields = array();
    foreach($result as $row) {
      $addressfields[] = $row->field_name;
    }
  }
  return $addressfields;
}

function _addressfield_link_render_address($format) {
  $address = $format['#address'];
  $format['link_block'] = array();

  if (!empty($address['link_title']) && !empty($address['link_url'])) {

    $format['link_block'] += array(
      '#type' => 'addressfield_container',
      '#attributes' => array(
        'class' => array('addressfield-link-block'),
      ),
      '#weight' => 200,
    );

    $format['link_block']['link'] = array(
      '#title' => t('Link'),
      '#type' => 'addressfield_container',
      '#attributes' => array('class' => array('link-title')),
      '#tag' => 'div',
      '#children' => l($address['link_title'], $address['link_url']),
    );

  }

  return $format;
}

/**
 * Implements hook_views_api().
 */
/**
 * WARNING!
 * Views integration is untested. What you see here is based on the
 * addressfield_phone module, from which addressfield_link was forked.
 * Uncomment and use at your own risk.
 *
function addressfield_link_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'addressfield_link') . '/views',
  );
}
//*/
